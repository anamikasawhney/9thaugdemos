import { Component, OnInit} from '@angular/core';
import { HelloService  } from '../hello.service';
import { OnSameUrlNavigation } from '@angular/router';
@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  constructor(private _helloService : HelloService)
  {}
 
  ngOnInit()
  {
    console.log(this._helloService.hello("Ajay"));
  }

}
