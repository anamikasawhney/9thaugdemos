import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
@Component({
  selector: 'app-asyncdemo',
  templateUrl: './asyncdemo.component.html',
  styleUrls: ['./asyncdemo.component.css']
})
export class AsyncdemoComponent implements OnInit {

  constructor() { }
  private mySubscription : any;

   

  // syntax of declaring promise

  ngOnInit(): void {
    const obj = new Promise((resolve, reject)=>
    {
      // console.log("Promise has been called 1")
     
      setTimeout(()=>
        {
          resolve("Promise1 has been called")
          resolve("Promise2 has been called")
          resolve("Promise3 has been called")
          resolve("Promise4 has been called")
        }, 1000);
      
    });
   
    // calling part of promise
    obj.then((res)=>
    {
      console.log(res)
  });

  // syntax for declaring observable
  var observable = new Observable(sub=>
    {
      // console.log("Observable called 1");
      setTimeout(()=>
      {
        sub.next("Observable1 called");
        sub.next("Observable2 called");
        sub.next("Observable3 called");
        sub.next("Observable2 called");
      }, 1000);
    })

    // caling part for observable
  //  observable.subscribe(res=>
  //   console.log(res));


    observable.pipe(
      filter(x=>x=="Observable2 called")
    ).subscribe(res=>
      console.log("Res is " + res));


      var observable1 = new Observable(sub=>
        {
            let x= 1;
      setInterval(()=>
        { x=x+1;
          sub.next(x);
        },1000);
}); 

this.mySubscription= observable1.subscribe(res=>
  console.log(res));
  }
  CancelSubscription()
  {
    this.mySubscription.unsubscribe();
  }

    
    


}
   
   




