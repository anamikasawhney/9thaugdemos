import { Injectable } from '@angular/core';
import { Employee } from 'src/models/employee';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

      employees: Employee[] = [];
    
  constructor() {
    
   }

   GetEmployees()
   {

    this.employees= [
      { id : 1, name:"Ajay", dept:"HR", salary:9000, doj:'10/12/1982'},
      
      { id : 2, name:"Ajay", dept:"HR", salary:9000, doj: '10/12/1982'},
      
      { id : 3, name:"Ajay", dept:"HR", salary:9000, doj: '10/12/1982'},
      
      { id : 4, name:"Ajay", dept:"HR", salary:9000, doj: new Date()},
      
      { id : 5, name:"Ajay", dept:"HR", salary:9000, doj: new Date()}
    ]
return this.employees;
      
   }
   AddEmployee (emp : Employee)
   {
    this.employees.push(emp);
    console.log(this.employees.length)
     
   }

   
   DeleteEmployee(id : number)
   {  
    let index = this.employees.findIndex(x=>x.id==id);
    console.log("p" + index);
      
      this.employees.splice(index,1);
      this.employees.forEach(element => {
        console.log(element)
      });

   }
   EditEmployee(id : number, employee : Employee)
   {  
     
    let index = this.employees.findIndex(x=>x.id==id);
    console.log("p" + index);
     
    console.log(employee)
     
     
    // console.log("POS" + temp)

    this.employees[index] = employee;
     
   }


}

