import { Component , OnInit } from '@angular/core';
import { PostService } from '../post.service';
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

   posts: any=[];
  constructor(private _postService: PostService){}
  ngOnInit(): void {
    //  this.posts = this._postService.getPosts();
    //  console.log(this.posts.length)

    this._postService.getPosts().subscribe(res =>
    {
      console.log(res);
      this.posts = res;
    });
  
}
}
