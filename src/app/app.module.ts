import { NgModule, importProvidersFrom } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HelloService } from './hello.service';
import { FirstComponent } from './first/first.component';
import { EmployeelistComponent } from './employeelist/employeelist.component';
import { EmployeeService } from './employee.service';
import { AsyncdemoComponent } from './asyncdemo/asyncdemo.component';
import { PostComponent } from './post/post.component';
import {HttpClientModule} from '@angular/common/http';
import { PostService } from './post.service';
import { ProductService } from './product.service';
import { ProductComponent } from './product/product.component';
@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    EmployeelistComponent,
    AsyncdemoComponent,
    PostComponent,
    ProductComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [HelloService, EmployeeService, PostService, ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
