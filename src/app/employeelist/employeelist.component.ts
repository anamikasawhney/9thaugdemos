import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Employee } from 'src/models/employee';
@Component({
  selector: 'app-employeelist',
  templateUrl: './employeelist.component.html',
  styleUrls: ['./employeelist.component.css']
})
export class EmployeelistComponent implements OnInit {
  constructor(private _empService : EmployeeService){}

  employees : Employee[] = [];
ngOnInit(): void {
  this.employees= this._empService.GetEmployees();
  // this.AddEmployee();
  // this.DeleteEmployee();
  // this.EditEmployee();
}

AddEmployee()
{
  var emp = new Employee();
  emp.id = 10;
  emp.name = "GAgan"
  emp.dept="new";
emp.doj=  new Date("2019-01-16"); 
   this._empService.AddEmployee(emp)
}

DeleteEmployee()
{
  this._empService.DeleteEmployee(10);
}

EditEmployee()
{

  var emp = new Employee();
  emp.id = 11;
  emp.name = "GAgan"
  this._empService.EditEmployee(1,emp);
}
}
