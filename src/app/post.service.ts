import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private _http : HttpClient) { }

   url : string ="https://jsonplaceholder.typicode.com/posts";
  getPosts()   : Observable<any>
  {
      return this._http.get<any[]>(this.url)
  }
}
