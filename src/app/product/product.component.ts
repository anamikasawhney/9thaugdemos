import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from 'src/models/product';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent  implements OnInit{
  constructor(private _productSecvice : ProductService){}
  products : Product[] = [];
   ngOnInit(): void {
     
    this._productSecvice.getProducts().subscribe(
      res => this.products = res
    )
   }

   AddProduct()
   {
    var product = new Product();
    product.id = 11;
    product.name="Mon";
    product.price= 9009;
      this._productSecvice.postProduct(product).subscribe(
        res => console.log(res))
   }

   DeleteProduct()
   {
    this._productSecvice.deleteProduct(1).subscribe(res=>
      console.log(res));
   }

   EditProduct()
   {
    var product = new Product();
    product.id = 2;
    product.name="new   product";
    product.price= 909089;
    this._productSecvice.editProduct(2, product).subscribe(res=>
      console.log(res));
   }
   GetProductById()
   {this._productSecvice.getProductById(10).subscribe(res=>
    console.log(res));

   }
}
