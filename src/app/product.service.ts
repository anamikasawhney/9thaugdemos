import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Product } from 'src/models/product';
import { Observable } from 'rxjs';
import { ReturnStatement } from '@angular/compiler';
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  products: Product[] = [];
  url : string="http://localhost:3000/products";
  constructor(private _http : HttpClient) { }
  getProducts() :Observable<Product[]>
  {
    return this._http.get<Product[]>(this.url);
  }


   postProduct(product : Product) : Observable<Product>
   {
    return this._http.post<Product>(this.url,
      JSON.stringify(product),
      {
        headers: new HttpHeaders({
        'Content-Type':'application/json',
        'Accept':'application/json'
        })
      });
    }
      deleteProduct(id: number) : Observable<Product>
      {
        return this._http.delete<Product>(this.url +"/"+ id);
      }
      editProduct(id: number, product: Product) : Observable<Product>
      {
        return this._http.put<Product>(this.url +"/"+ id,
        JSON.stringify(product),{
          headers: new HttpHeaders({
          'Content-Type':'application/json',
          'Accept':'application/json'
          })
        });
      }

      getProductById(id: number) : Observable<Product>
      {
        return this._http.get<Product>(this.url +"/"+ id);
      }


}
